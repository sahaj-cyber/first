import { toast } from 'react-toastify'

function showSuccess(msg) {
    return toast.success(msg);
}

function showInfo(msg) {
    return toast.info(msg)
}

function showWarning(msg) {
    return toast.warning(msg)
}

function handleError(msg) {
    debugger;
    const errors = error.response.data;
    let errMsg;
    if (errors && errors.msg) {
        errMsg = errors.msg;
    }
    showError(errMsg);
}

export default {
    showSuccess,
    showInfo,
    showWarning,
    handleError
}