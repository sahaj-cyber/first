const Dashboard = props => {
    return (
        <div>
            <h2> Dashboard </h2>
            <p> Welcome to our store</p>
        </div>
    )
}

const ProtectedRoute = ({component:Component, ...rest}) =>{
    return <Route {...rest} render={(routeProps)=>(
        localStorage.getItem('token')
        ?<div>
<Component {...routeProps}></Component>
        </div>
        : <Redirect to="/"></Redirect>
    )}
}

const Routing = props => {
    return <Router>
        <Header is logged
    </Router>
}