import React from 'react';
import './header.component.css'
import { Link } from 'react-router-dom'

export const Header = (props) => {
    
    let menu = props.isLoggedIn

        ? <ul className="nav_list">
            <li className="nav_item">
                <Link to="/dashboard/asfas"> Home </Link>
            </li>
            <li className="nav_item">
                <Link to="/Profile"> Profile </Link>
            </li>
            <li className="nav_item float_right">
                <Link to="/"> Log out </Link>
            </li>
        </ul>
        :

        <ul className="nav_list">
            <li className="nav_item"> Home </li>
            <li className="nav_item"> Login </li>
            <li className="nav_item"> Register </li>
            <li className="nav_item"> Profile </li>
            <li className="nav_item"> Log out </li>
        </ul>
        

    return (
        <div className="nav_bar">
            {menu}
        </div>
    )
}