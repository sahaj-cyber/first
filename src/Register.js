import React, { Component } from 'react'
import style from './style.css'

const Defaultform = {
    name: '',
    email: '',
    phoneNumber: '',
    username: '',
    password: '',
    gender: '',
    dob: ''

}

class Register extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: {
                ...Defaultform
            },
            error: {
                ...Defaultform
            },
            isSubmitting: false,
            isValidForm: false
        }
    }

    handleChange = (e) => {
        const { name, value } = e.target
        this.setState(prevState => ({
            data: {
                ...prevState.data,
                [name]: value
            }
        }), () => {
            this.ValidateForm(name)
        })
    }

    ValidateForm(fieldname) {
        let errMsg;
        switch (fieldname) {
            case 'username':
                errMsg = this.state.data[fieldname]
                    ? ''
                    : 'Required field'
                break;
            default:
                break;
        }


        this.setState(prevState => ({
            error: {
                ...prevState.error,
                [fieldname]: errMsg
            }
        }))
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({ isSubmitting: true})

        setTimeout(()=>{
            this.setState({isSubmitting: false})
        },4000)
    }
    render() {
        return (
            <div>
                <h2> Register </h2>
                <p className='error'> Please Register to continue </p>
                <form className='form-group' onSubmit={this.handleSubmit}>
                    <label> Name </label>
                    <input className='form-control' type='text' placeholder='Name' name='name' onChange={this.handleChange}></input>
                    <p className='error'>{this.state.error.name} </p>

                    <label> Email </label>
                    <input className='form-control' type='email' placeholder='Email' name='email' onChange={this.handleChange}></input>
                    <p className='error'>{this.state.error.email} </p>

                    <label> Phone Number </label>
                    <input className='form-control' type='number' placeholder='Phone Number' name='phoneNumber' onChange={this.handleChange}></input>
                    <p>{this.state.error.phoneNumber} </p>

                    <label> Username </label>
                    <input className='form-control' type='text' placeholder='Username' name='username' onChange={this.handleChange}></input>
                    <p>{this.state.error.username} </p>

                    <label> Password </label>
                    <input className='form-control' type='password' placeholder='Password' name='password' onChange={this.handleChange}></input>
                    <p>{this.state.error.password} </p>

                    <label> Address </label>
                    <input className='form-control' type='text' placeholder='Address' name='address' onChange={this.handleChange}></input>
                    <p>{this.state.error.address} </p>

                    <label> Gender </label>
                    <input className='form-control' type='text' placeholder='Gender' name='gender' onChange={this.handleChange}></input>
                    <p >{this.state.error.gender} </p>

                    <label> Date of Birth </label>
                    <input className='form-control' type='text' placeholder='DOB' name='dob' onChange={this.handleChange}></input><br />

                    <button className='btn btn-primary' type='submit'> Submit </button>
                </form>

            </div>
        )
    }
}
export default Register;