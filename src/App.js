import React from 'react';
import logo from './logo.svg';
import './App.css';
import Register from './Register'
import { Header } from './common/header.component'

function App() {
  return (
<div>
      <Header isLoggedIn={false}></Header>
   <Register/>   

    </div>
  )
}

export default App;
